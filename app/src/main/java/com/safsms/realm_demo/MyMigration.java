package com.safsms.realm_demo;

import android.support.annotation.NonNull;

import com.safsms.realm_demo.models.Company;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

class MyMigration implements RealmMigration {

    @Override
    public void migrate(@NonNull DynamicRealm realm, long oldVersion, long newVersion) {

        RealmSchema schema = realm.getSchema();
        if (oldVersion == 0) {
            RealmObjectSchema objectSchema = schema.get("User");
            if (objectSchema != null) {
                objectSchema.addField("salary", String.class, FieldAttribute.REQUIRED);
            }
            oldVersion++;
        }

        if (oldVersion == 1) {
            RealmObjectSchema companySchema = schema.create("Company");
            companySchema.addField("companyName", String.class);
            companySchema.addField("companyAddress", String.class);

            RealmObjectSchema userSchema = schema.get("User");
            if (userSchema != null) {
                userSchema.addRealmObjectField("company", companySchema);
                /**For non-primitive data objects, use .addRealmObjectField(),
                 * and .addField() for primitive data types*/
            }
            oldVersion++;
        }
    }
}
