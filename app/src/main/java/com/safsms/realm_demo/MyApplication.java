package com.safsms.realm_demo;

import android.app.Application;

import com.safsms.realm_demo.models.Company;
import com.safsms.realm_demo.models.SocialAccount;
import com.safsms.realm_demo.models.User;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.annotations.RealmModule;

public class MyApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        Realm.init(this);
        RealmConfiguration configuration = new RealmConfiguration.Builder()
                .name("firstRealmDB.realm")
                .modules(new CustomModule())
                .schemaVersion(2) //schema upgraded incrementally from 0 for every migration
                .migration(new MyMigration())
                .build();
        Realm.setDefaultConfiguration(configuration);
    }

    @RealmModule (classes = {User.class, SocialAccount.class, Company.class})
    private class CustomModule {

    }
}
