package com.safsms.realm_demo;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.safsms.realm_demo.models.SocialAccount;
import com.safsms.realm_demo.models.User;
import java.util.UUID;
import io.realm.Case;
import io.realm.Realm;
import io.realm.RealmAsyncTask;
import io.realm.RealmResults;
import io.realm.Sort;

public class MainActivity extends AppCompatActivity {

    private EditText nameField, ageField, socialAccountField, statusField;
    private Realm realmDB;
    private RealmAsyncTask realmAsyncTask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        realmDB = Realm.getDefaultInstance();
        nameField = findViewById(R.id.nameTextField);
        ageField = findViewById(R.id.ageTextField);
        socialAccountField = findViewById(R.id.socialAccountTextField);
        statusField = findViewById(R.id.statusTextField);
    }

    public void addSynchronously(View view) {

        final String id = UUID.randomUUID().toString();
        final String name = nameField.getText().toString().trim();
        final int age = Integer.parseInt(ageField.getText().toString().trim());
        final String socialAccount = socialAccountField.getText().toString().trim();
        final String status = statusField.getText().toString().trim();

        realmDB.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                SocialAccount social = realm.createObject(SocialAccount.class);
                social.setName(socialAccount);
                social.setStatus(status);
                User user = realm.createObject(User.class, id);
                user.setName(name);
                user.setAge(age);
                user.setSocialAccount(social);
                Toast.makeText(MainActivity.this, "Added Successfully",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addAsynchronously(View view) {

        final String id = UUID.randomUUID().toString();
        final String name = nameField.getText().toString().trim();
        final int age = Integer.parseInt(ageField.getText().toString().trim());
        final String socialAccount = socialAccountField.getText().toString().trim();
        final String status = statusField.getText().toString().trim();

        realmAsyncTask = realmDB.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {
                SocialAccount social = realm.createObject(SocialAccount.class);
                social.setName(socialAccount);
                social.setStatus(status);
                User user = realm.createObject(User.class, id);
                user.setName(name);
                user.setAge(age);
                user.setSocialAccount(social);
//
//                List<User> userList = new LinkedList<>();
//                RealmList<User> realmList = new RealmList<>();
//                realmList.addAll(userList);
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Toast.makeText(MainActivity.this, "Added Async Successfully",
                        Toast.LENGTH_SHORT).show();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(@NonNull Throwable error) {
                Toast.makeText(MainActivity.this, "Error Saving",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (realmAsyncTask != null && !realmAsyncTask.isCancelled()) {
            realmAsyncTask.cancel();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        realmDB.close();
    }

    public void displayData(View view) {
       RealmResults<User> userList = realmDB.where(User.class).findAll();
       displayQueriedUsers(userList);
    }

    public void displayQueriedUsers(RealmResults<User> userList) {

        StringBuilder builder = new StringBuilder();
        for (User user : userList) {
            builder.append("\n\nID: ").append(user.getId());
            builder.append("\nname: ").append(user.getName());
            builder.append("\nage: ").append(user.getAge());

            SocialAccount socialAccount = user.getSocialAccount();
            builder.append("\n account: ").append(socialAccount.getName());
            builder.append("\n status: ").append(socialAccount.getStatus()).append("\n\n");
        }
        System.out.println(builder.toString());
    }

    public void sampleQuery(View view) {
    /**    RealmQuery<User> realmQuery = realmDB.where(User.class);
        realmQuery.greaterThan("age", 10);
        realmQuery.contains("name", "Yerimah");
        RealmResults<User> userList = realmQuery.findAll();**/

        //fluid Query:
        RealmResults<User> realmResults = realmDB.where(User.class)
                .greaterThan("Age", 10)
                .between("age", 11, 20)
                .contains("name", "Yer", Case.INSENSITIVE)
                .findAll()
                .sort("socialAccount.name", Sort.ASCENDING);

        displayQueriedUsers(realmResults);
    }

    public void updateAndDelete(View view) {

        final String id = UUID.randomUUID().toString();
        final String name = nameField.getText().toString().trim();
        final int age = Integer.parseInt(ageField.getText().toString().trim());
        final String socialAccount = socialAccountField.getText().toString().trim();
        final String status = statusField.getText().toString().trim();

        realmAsyncTask = realmDB.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(@NonNull Realm realm) {

                User firstUser = realm.where(User.class).findFirst();
                assert firstUser != null;
                firstUser.setAge(age);
                firstUser.setName(name);

                SocialAccount firstUserSocialAccount = firstUser.getSocialAccount();
                if (firstUserSocialAccount != null) {
                    firstUserSocialAccount.setStatus(status);
                    firstUserSocialAccount.setName(socialAccount);

                    /** To delete after obtaining User Object, invoke:
                     * firstUser.deleteFromRealm();
                     *
                     * for Results"List of objects", :
                     * userList.deleteFirstFromRealm();,
                     * deleteLast,
                     * deleteFromRealm(index); and
                     * deleteAllFromRealm();*/

                }
            }
        }, new Realm.Transaction.OnSuccess() {
            @Override
            public void onSuccess() {
                Toast.makeText(MainActivity.this, "Update Successful",
                        Toast.LENGTH_SHORT).show();
            }
        }, new Realm.Transaction.OnError() {
            @Override
            public void onError(@NonNull Throwable error) {
                Toast.makeText(MainActivity.this, "Update Error",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void loadSecondActivity(View view) {
        Intent intent = new Intent(MainActivity.this, ConceptsActivity.class);
        startActivity(intent);
        finish();
    }
}
