package com.safsms.realm_demo;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import com.safsms.realm_demo.models.User;
import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

public class ConceptsActivity extends AppCompatActivity {

    private Realm realmDB;
    private MainActivity activity = new MainActivity();
    private RealmResults<User> userList;

    RealmChangeListener<RealmResults<User>> userListener = new
            RealmChangeListener<RealmResults<User>>() {
        @Override
        public void onChange(@NonNull RealmResults<User> userList) {
            activity.displayQueriedUsers(userList);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_concepts);
        realmDB = Realm.getDefaultInstance();
    }

    public void initMiscellaneousConcept(View view) {

        userList = realmDB.where(User.class).findAllAsync();
        userList.addChangeListener(userListener);
    }

    @Override
    protected void onStop() {
        super.onStop();

        if (userList != null) {
            userList.removeAllChangeListeners();
        }
    }
}
