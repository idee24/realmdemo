package com.safsms.realm_demo.models;

import io.realm.RealmObject;

public class SocialAccount extends RealmObject {

    private String name;
    private String status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "SocialAccount{" +
                "name='" + name + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
